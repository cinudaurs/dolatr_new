package com.dolatr.app.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.account.UserAccountServiceImpl;
import com.dolatr.app.account.UserAdminService;
import com.dolatr.app.account.UserAdminServiceImpl;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.repository.UserSocialConnectionRepository;
import com.dolatr.app.system.CounterService;
import com.dolatr.app.system.CounterServiceImpl;

@Configuration
@ComponentScan(basePackages = "com.dolatr.app")
@PropertySource("classpath:application.properties")
class MainAppConfig {
    @Inject
    private Environment environment;

    //Repository beans injected from MongoConfig
    @Inject
    private UserAccountRepository accountRepository;
    @Inject
    private UserSocialConnectionRepository userSocialConnectionRepository;
    

    //Application Service beans
    @Bean
    public UserAccountService accountService(MongoTemplate mongoTemplate, UserAccountRepository accountRepository,
                    UserSocialConnectionRepository userSocialConnectionRepository) {
        UserAccountServiceImpl service = new UserAccountServiceImpl(accountRepository, userSocialConnectionRepository,
                userAdminService(mongoTemplate));
        return (UserAccountService) service;
    }

    @Bean
    public UserAdminService userAdminService(MongoTemplate mongoTemplate) {
        return new UserAdminServiceImpl(accountRepository, counterService(mongoTemplate));
    }

    @Bean
    public CounterService counterService(MongoTemplate mongoTemplate) {
        return new CounterServiceImpl(mongoTemplate);
    }

}
