package com.dolatr.app.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.social.security.SpringSocialConfigurer;

import com.dolatr.app.repository.RememberMeTokenRepository;

@ComponentScan
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Inject
	private Environment environment;

	@Inject
	RememberMeTokenRepository rememberMeTokenRepository;

	@Bean
	public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
		RememberMeAuthenticationProvider rememberMeAuthenticationProvider = new RememberMeAuthenticationProvider(
				environment.getProperty("application.key"));
		return rememberMeAuthenticationProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().formLogin()
				.loginPage("/signin")
				.failureUrl("/signin?param.error=bad_credentials")
				.loginProcessingUrl("/signin").permitAll()
				.defaultSuccessUrl("/connect").and().logout()
				.logoutUrl("/signout").permitAll();
		http.apply(new SpringSocialConfigurer());
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}

}
