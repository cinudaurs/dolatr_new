package com.dolatr.app.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SocialAuthenticationProvider;
import org.springframework.social.security.SocialAuthenticationServiceLocator;
import org.springframework.social.security.SocialAuthenticationServiceRegistry;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.social.security.provider.OAuth1AuthenticationService;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import com.dolatr.app.account.AccountUtils;
import com.dolatr.app.account.SimpleSocialUserDetailsService;
import com.dolatr.app.account.UserAdminService;
import com.dolatr.app.controller.AutoConnectionSignUp;
import com.dolatr.app.controller.SignupController;
import com.dolatr.app.controller.SimpleSignInAdapter;
import com.dolatr.app.controller.twitter.TweetAfterConnectInterceptor;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.RememberMeTokenRepository;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.repository.UserSocialConnectionRepository;
import com.dolatr.app.repository.impl.MongoPersistentTokenRepositoryImpl;
import com.dolatr.app.repository.impl.MongoUsersConnectionRepositoryImpl;

@Configuration
@EnableSocial
public class SocialConfig extends SocialConfigurerAdapter {

	@Inject
	UserAdminService userAdminService;

	@Inject
	private Environment environment;

	@Inject
	SocialUserDetailsService socialUserDetailsService;

	@Inject
	UserDetailsService userDetailsService;

	@Inject
	private AuthenticationManager authenticationManager;

	@Inject
	private RememberMeTokenRepository rememberMeTokenRepository;

	@Inject
	private UserAccountRepository userAccountRepository;

	@Bean
	public LoginUrlAuthenticationEntryPoint socialAuthenticationEntryPoint() {
		return new LoginUrlAuthenticationEntryPoint("/signin");
	}

	/**
	 * Singleton data access object providing access to connections across all
	 * users.
	 */
	@Bean
	public UsersConnectionRepository usersConnectionRepository(
			UserSocialConnectionRepository userSocialConnectionRepository) {
		MongoUsersConnectionRepositoryImpl repository = new MongoUsersConnectionRepositoryImpl(
				userSocialConnectionRepository,
				socialAuthenticationServiceLocator(), Encryptors.noOpText());
	//	repository.setConnectionSignUp(autoConnectionSignUp());
		return repository;
	}

	@Bean
	public SocialAuthenticationServiceLocator socialAuthenticationServiceLocator() {
		SocialAuthenticationServiceRegistry registry = new SocialAuthenticationServiceRegistry();

		// add twitter
		OAuth1ConnectionFactory<Twitter> twitterConnectionFactory = new TwitterConnectionFactory(
				environment.getProperty("twitter.consumerKey"),
				environment.getProperty("twitter.consumerSecret"));
		OAuth1AuthenticationService<Twitter> twitterAuthenticationService = new OAuth1AuthenticationService<Twitter>(
				twitterConnectionFactory);
		registry.addAuthenticationService(twitterAuthenticationService);

		return registry;
	}

	
	@Bean
	@Scope(value="singleton", proxyMode=ScopedProxyMode.INTERFACES)
    public ConnectionFactoryLocator connectionFactoryLocator() {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
       
        registry.addConnectionFactory(new TwitterConnectionFactory(
            environment.getProperty("twitter.consumerKey"),
            environment.getProperty("twitter.consumerSecret")));
            
        return registry;
    }
	
	

	@Override
	public void addConnectionFactories(
			ConnectionFactoryConfigurer connectionFactoryConfigurer,
			Environment env) {

		connectionFactoryConfigurer
				.addConnectionFactory(new TwitterConnectionFactory(env
						.getRequiredProperty("twitter.consumerKey"), env
						.getRequiredProperty("twitter.consumerSecret")));
	}

	@Bean
	public ConnectionSignUp autoConnectionSignUp() {
		return new AutoConnectionSignUp(userAdminService);
	}

	/**
	 * Request-scoped data access object providing access to the current user's
	 * connections.
	 */
	@Bean
	@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
	public ConnectionRepository connectionRepository(
			UserSocialConnectionRepository userSocialConnectionRepository) {
		
		AccountUtils accountUtils = new AccountUtils(userAccountRepository);
		
		UserAccount user = accountUtils.getLoginUserAccount();
		return usersConnectionRepository(userSocialConnectionRepository)
				.createConnectionRepository(user.getUserId());
	}

	@Bean
	@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
	public Twitter twitter(ConnectionRepository repository) {
		Connection<Twitter> connection = repository
				.findPrimaryConnection(Twitter.class);
		return connection != null ? connection.getApi() : new TwitterTemplate(
				environment.getProperty("twitter.consumerKey"),
				environment.getProperty("twitter.consumerSecret"),
				environment.getProperty("twitter.accessToken"),
				environment.getProperty("twitter.accessTokenSecret"));
	}

	@Bean
	public SocialAuthenticationFilter socialAuthenticationFilter(
			UserSocialConnectionRepository userSocialConnectionRepository) {
		SocialAuthenticationFilter filter = new SocialAuthenticationFilter(
				authenticationManager, userAdminService,
				usersConnectionRepository(userSocialConnectionRepository),
				socialAuthenticationServiceLocator());
		filter.setFilterProcessesUrl("/signin");
		filter.setConnectionAddedRedirectUrl("/myAccount");
		filter.setPostLoginUrl("/connect"); // ??? Remove it?
		filter.setRememberMeServices(rememberMeServices());
		return filter;
	}

	@Bean
	public SocialUserDetailsService socialUserDetailsService() {
		return new SimpleSocialUserDetailsService();
	}

	@Bean
	public ConnectController connectController(
			ConnectionFactoryLocator connectionFactoryLocator,
			ConnectionRepository connectionRepository) {
		ConnectController connectController = new ConnectController(
				connectionFactoryLocator, connectionRepository);

		connectController.addInterceptor(new TweetAfterConnectInterceptor());
		return connectController;
	}

	
	@Bean
	public SignupController signupController(
			ConnectionFactoryLocator connectionFactoryLocator,
			UsersConnectionRepository connectionRepository) {
		SignupController signupController = new SignupController(userAdminService,
				connectionFactoryLocator, connectionRepository);
		
		return signupController;
	}
	
	
	@Bean
	public RememberMeServices rememberMeServices() {
		PersistentTokenBasedRememberMeServices rememberMeServices = new PersistentTokenBasedRememberMeServices(
				environment.getProperty("application.key"), userDetailsService,
				persistentTokenRepository());
		rememberMeServices.setAlwaysRemember(true);
		return rememberMeServices;
	}

	@Bean
	public SocialAuthenticationProvider socialAuthenticationProvider(
			UserSocialConnectionRepository userSocialConnectionRepository) {
		return new SocialAuthenticationProvider(
				usersConnectionRepository(userSocialConnectionRepository),
				socialUserDetailsService);
	}
	
	@Bean
	public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository usersConnectionRepository) {
		ProviderSignInController providerSignInController = new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository, new SimpleSignInAdapter(new HttpSessionRequestCache()));
		//providerSignInController.setPostSignInUrl("/myAccount");
		
		return providerSignInController;
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		return new MongoPersistentTokenRepositoryImpl(rememberMeTokenRepository);
	}

	@Override
	public UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}

}
