package com.dolatr.app.system;

/**
 * Service interface for counter functions.
 */
public interface CounterService {
    
    long getNextUserIdSequence();
    
    long logVisit();
    
    long getVisitCount();
   
}
