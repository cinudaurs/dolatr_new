package com.dolatr.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.dolatr.app.documents.RememberMeToken;

/**
 * MongoDB Repository for RememberMeToken entity.
 */
public interface RememberMeTokenRepository extends MongoRepository<RememberMeToken, String>{
    
    RememberMeToken findBySeries(String series);
    
    List<RememberMeToken> findByUsername(String username);
}
