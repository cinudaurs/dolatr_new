package com.dolatr.app.repository;

import java.util.Collection;
import java.util.List;



import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.dolatr.app.documents.UserSocialConnection;

/**
 * MongoDB Repository for UserSocialConnection entity.
 */
public interface UserSocialConnectionRepository extends MongoRepository<UserSocialConnection, String>{
    
	List<UserSocialConnection> findByUserId(String userId);
	
    List<UserSocialConnection> findByUserId(String userId, Sort sort);
        
    List<UserSocialConnection> findByUserIdAndProviderId(String userId, String providerId);
    
    List<UserSocialConnection> findByUserIdAndProviderId(String userId, String providerId, Sort sort);
    
    List<UserSocialConnection> findByProviderIdAndProviderUserId(String providerId, String providerUserId);
    
    UserSocialConnection findByUserIdAndProviderIdAndProviderUserId(String userId, String providerId, String providerUserId);
    
    List<UserSocialConnection> findByProviderIdAndProviderUserIdIn(String providerId, Collection<String> providerUserIds);
}
