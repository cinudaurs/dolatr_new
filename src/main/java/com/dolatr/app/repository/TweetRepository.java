package com.dolatr.app.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.dolatr.app.documents.Tweet;


/**
 * MongoDB Repository for SinceId entity.
 */
public interface TweetRepository extends MongoRepository<Tweet, String> {

	Tweet findByTweetId(Long tweetId);
    
    Page<Tweet> findAllOrderByTweetId(Pageable pageable);
    
}



	
	
    
    
    
