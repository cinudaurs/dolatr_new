package com.dolatr.app.documents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.social.twitter.api.Entities;
import org.springframework.social.twitter.api.TwitterProfile;

import twitter4j.HashtagEntity;

@Document(collection = "Tweet")
public class Tweet extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Indexed
	private final Long tweetId;
	
	private final String text;
	private final Date createdAt;
	private String fromUser;
	private String profileImageUrl;
	private Long toUserId;
	private Long inReplyToStatusId;
	private Long inReplyToUserId;
	private String inReplyToScreenName;
	private Long fromUserId;
	private String languageCode;
	private String source;
	private Integer retweetCount;
	private boolean retweeted;
	private Tweet retweetedStatus;
	private boolean favorited;
	private Integer favoriteCount;
	private TwitterProfile user;
	private Entities entities;
	
	private ArrayList<String> hashTags;
	
	public Tweet(Long tweetId, String text, Date createdAt, String fromUser, String profileImageUrl, Long toUserId, Long fromUserId) {
		
		this.tweetId = tweetId;
		this.text = text;
		this.createdAt = createdAt;
		this.fromUser = fromUser;
		this.profileImageUrl = profileImageUrl;
		this.toUserId = toUserId;
		this.fromUserId = fromUserId;
		
	}

	/**
	 * The text of the tweet. If this tweet is a retweet of another tweet, the text may be preceeded with "RT \@someuser" and may be truncated at the end.
	 * To get the raw, unmodified text of the original tweet, use {@link #getUnmodifiedText()}. 
	 * @return The text of the tweet.
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Returns the unmodified text of the tweet.
	 * If this tweet is a retweet, it returns the text of the original tweet.
	 * If it is not a retweet, then this method will return the same value as {@link #getText()}.
	 * @return The unmodified text of the tweet.
	 */
	public String getUnmodifiedText() {
		return isRetweet() ? retweetedStatus.getText() : getText();
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	
	public Long getTweetId() {
		return tweetId;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public Long getFromUserId() {
		return fromUserId;
	}
	
	public void setInReplyToStatusId(Long inReplyToStatusId) {
		this.inReplyToStatusId = inReplyToStatusId;
	}
	
	public Long getInReplyToStatusId() {
		return inReplyToStatusId;
	}

	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;		
	}
	
	/**
	 * The number of times this tweet has been retweeted.
	 * Only available in timeline results. 
	 * getRetweetCount() will return null for Tweet objects returned in search results.
	 */
	public Integer getRetweetCount() {
		return retweetCount;
	}
	
	public void setRetweeted(boolean retweeted) {
		this.retweeted = retweeted;
	}

	public boolean isRetweeted() {
		return retweeted;
	}
	
	public Tweet getRetweetedStatus() {
		return this.retweetedStatus;
	}

	public void setRetweetedStatus(final Tweet tweet) {
		this.retweetedStatus = tweet;
	}
	
	public boolean isRetweet() {
		return this.retweetedStatus != null;
	}

	public void setFavorited(boolean favorited) {
		this.favorited = favorited;
	}

	public boolean isFavorited() {
		return favorited;
	}
	
	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	
	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public Entities getEntities() {
		return this.entities;
	}
	
	public void setEntities(final Entities ent) {
		this.entities = ent;
	}
	
	public ArrayList<String> getHashTags() {
		return this.hashTags;
	}
	
	public void setHashTags(ArrayList<String> hashTags) {
		this.hashTags = hashTags;
	}
	

	public boolean hasMentions() {
		if (this.entities == null) {
			return false;
		}
		return !this.entities.getMentions().isEmpty();
	}

	public boolean hasMedia() {
		if (this.entities == null) {
			return false;
		}
		return !this.entities.getMedia().isEmpty();
	}

	public boolean hasUrls() {
		if (this.entities == null) {
			return false;
		}
		return !this.entities.getUrls().isEmpty();
	}

	public boolean hasTags() {
		if (this.entities == null) {
			return false;
		}
		return !this.entities.getHashTags().isEmpty();
	}
	
	public TwitterProfile getUser() {
		return this.user;
	}
	
	public void setUser(final TwitterProfile prof) {
		this.user = prof;
	}

	public Long getInReplyToUserId() {
		return inReplyToUserId;
	}

	public void setInReplyToUserId(final Long inReplyToUserId) {
		this.inReplyToUserId = inReplyToUserId;
	}

	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}

	public void setInReplyToScreenName(final String inReplyToScreenName) {
		this.inReplyToScreenName = inReplyToScreenName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Tweet tweet = (Tweet) o;

		if (fromUserId != tweet.fromUserId) {
			return false;
		}
		if (tweetId != tweet.getTweetId()) {
			return false;
		}
		if (retweeted != tweet.retweeted) {
			return false;
		}
		if (createdAt != null ? !createdAt.equals(tweet.createdAt) : tweet.createdAt != null) {
			return false;
		}
		if (entities != null ? !entities.equals(tweet.entities) : tweet.entities != null) {
			return false;
		}
		if (fromUser != null ? !fromUser.equals(tweet.fromUser) : tweet.fromUser != null) {
			return false;
		}
		if (inReplyToScreenName != null ? !inReplyToScreenName.equals(tweet.inReplyToScreenName) : tweet.inReplyToScreenName != null) {
			return false;
		}
		if (inReplyToStatusId != null ? !inReplyToStatusId.equals(tweet.inReplyToStatusId) : tweet.inReplyToStatusId != null) {
			return false;
		}
		if (inReplyToUserId != null ? !inReplyToUserId.equals(tweet.inReplyToUserId) : tweet.inReplyToUserId != null) {
			return false;
		}
		if (languageCode != null ? !languageCode.equals(tweet.languageCode) : tweet.languageCode != null) {
			return false;
		}
		if (profileImageUrl != null ? !profileImageUrl.equals(tweet.profileImageUrl) : tweet.profileImageUrl != null) {
			return false;
		}
		if (retweetCount != null ? !retweetCount.equals(tweet.retweetCount) : tweet.retweetCount != null) {
			return false;
		}
		if (retweetedStatus != null ? !retweetedStatus.equals(tweet.retweetedStatus) : tweet.retweetedStatus != null) {
			return false;
		}
		if (source != null ? !source.equals(tweet.source) : tweet.source != null) {
			return false;
		}
		if (text != null ? !text.equals(tweet.text) : tweet.text != null) {
			return false;
		}
		if (toUserId != null ? !toUserId.equals(tweet.toUserId) : tweet.toUserId != null) {
			return false;
		}
		if (user != null ? !user.equals(tweet.user) : tweet.user != null) {
			return false;
		}
	
		return true;
	}

	
}
