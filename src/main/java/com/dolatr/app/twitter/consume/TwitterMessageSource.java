package com.dolatr.app.twitter.consume;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.inject.Inject;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Sort;
import org.springframework.integration.core.MessageSource;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import com.dolatr.app.documents.Tweet;

import org.springframework.util.Assert;

import com.dolatr.app.documents.UserSocialConnection;
import com.dolatr.app.repository.TweetRepository;

import twitter4j.HashtagEntity;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class TwitterMessageSource implements MessageSource<Tweet>,
		InitializingBean {

	static private Logger logger = Logger.getLogger(TwitterMessageSource.class);

	private volatile Twitter twitter;
	@Inject
	private Environment environment;

	@Inject
	private TweetRepository tweetRepository;
	
	List<Status> statusesNew;

	List<Status> statusesOld;

	private volatile long lastSinceIdRetrieved;

	private long lastMaxIdRetrieved;

	private Tweet tweet;

	private Paging paging;

	private Tweet buildTweetFromStatus(Status firstPost) {
		
		ArrayList<String> hashTags = new ArrayList<String>();
		
		if(firstPost.getHashtagEntities().length > 0){		
			
			HashtagEntity[] hashTagEntities = firstPost.getHashtagEntities();
					
			for(HashtagEntity hashTag: hashTagEntities)
			{
				
				String hashTagText = hashTag.getText();
				hashTags.add(hashTagText);
			}
			
		}
		
				
		String tweetTextNoHashTags = removeTags(firstPost.getText(), hashTags);
		
			tweet = new Tweet(firstPost.getId(), tweetTextNoHashTags,
					firstPost.getCreatedAt(), firstPost.getUser().getName(), null,
					null, firstPost.getUser().getId());
			
			tweet.setHashTags(hashTags);
		
		return tweet;

	}

	private String removeTags(String tweetText, ArrayList<String> hashTags) {
		
		Set<String> hashTagsSet = new HashSet<String>();
		
		for(String tag: hashTags){
		
			tag = "#"+tag;
			hashTagsSet.add(tag);
			
		}
		
				
		
		StringBuffer clean = new StringBuffer();
		int index = 0;

		while (index < tweetText.length()) {
		  // the only word delimiter supported is space, if you want other
		  // delimiters you have to do a series of indexOf calls and see which
		  // one gives the smallest index, or use regex
		  int nextIndex = tweetText.indexOf(" ", index);
		  if (nextIndex == -1) {
		    nextIndex = tweetText.length() ;
		  }
		  String word = tweetText.substring(index, nextIndex);
		  if (!hashTagsSet.contains(word)) {
		    clean.append(" "+word+" ");
		  }
		  index = nextIndex + 1;
		}
		
		return clean.toString();
		
	}

	public Message<Tweet> receive() {

		lastSinceIdRetrieved = getLastSinceIdRetrieved();

		if (lastSinceIdRetrieved > 0) {
					
			try {
				paging = new Paging();
				
						paging.setSinceId(lastSinceIdRetrieved);
						paging.setCount(300);
						statusesNew = twitter.getUserTimeline(paging);
						buildAndStoreTweets(statusesNew);
						
				}
				catch (TwitterException e) {

				logger.info("<<API_RATE_LIMIT_HIT>>"
						+ "will wait until scheduled cron rerun" + " "
						+ ExceptionUtils.getFullStackTrace(e));
				System.out.println("<<API_RATE_LIMIT_HIT>>");
			}

		}

		lastMaxIdRetrieved = getLastMaxIdRetrieved();

		System.out.println("last max id:" + lastMaxIdRetrieved);

		if (lastMaxIdRetrieved > 0) {

			paging = new Paging();

			lastMaxIdRetrieved -= 1;

			paging.setMaxId(lastMaxIdRetrieved);
			paging.setCount(300);

			try {

				statusesOld = twitter.getUserTimeline(paging);
				buildAndStoreTweets(statusesOld);

			} catch (TwitterException e) {
				logger.info("<<API_RATE_LIMIT_HIT>>"
						+ "will wait until scheduled cron rerun" + " "
						+ ExceptionUtils.getFullStackTrace(e));
				System.out.println("<<API_RATE_LIMIT_HIT>>");

			}

		}

		if (lastMaxIdRetrieved == -1 || lastSinceIdRetrieved == -1) {
			try {
				paging = new Paging();
				paging.setCount(300);
				List<Status> statuses = twitter.getUserTimeline(paging);
				buildAndStoreTweets(statuses);

			} catch (TwitterException e) {
				logger.info("<<API_RATE_LIMIT_HIT>>"
						+ "will wait until scheduled cron rerun" + " "
						+ ExceptionUtils.getFullStackTrace(e));
				System.out.println("<<API_RATE_LIMIT_HIT>>");
			}
		}

		return null;

	}

	private void buildAndStoreTweets(List<Status> statuses) {

		if (!statuses.isEmpty()) {
			for (Status status : statuses) {
				tweet = buildTweetFromStatus(status);
				this.tweetRepository.save(tweet);
			}
		}
	}

	public void afterPropertiesSet() throws Exception {

		if (twitter == null) {

			TwitterFactory factory = new TwitterFactory();

			twitter = factory.getInstance();

			twitter.setOAuthConsumer(
					environment.getProperty("twitter.consumerKey"),
					environment.getProperty("twitter.consumerSecret"));
			twitter.setOAuthAccessToken(new AccessToken(environment
					.getProperty("twitter.accessToken"), environment
					.getProperty("twitter.accessTokenSecret")));

		}

		lastMaxIdRetrieved = -1;
		lastSinceIdRetrieved = -1;

	}

	private long getLastMaxIdRetrieved() {

		List<Tweet> maxIdList = (List<Tweet>) this.tweetRepository
				.findAll(new Sort(Sort.Direction.ASC, "tweetId"));

		if (maxIdList.size() != 0)
			return maxIdList.get(0).getTweetId();

		return -1;

	}

	private long getLastSinceIdRetrieved() {

		List<Tweet> sinceIdList = (List<Tweet>) this.tweetRepository
				.findAll(new Sort(Sort.Direction.DESC, "tweetId"));

		if (sinceIdList.size() != 0)
			return sinceIdList.get(0).getTweetId();

		return -1;

	}

}
