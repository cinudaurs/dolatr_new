package com.dolatr.app.twitter.consume;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Component;

@Component
public class TwitterMessageConsumer {
    private static Logger LOG = Logger.getLogger(TwitterMessageConsumer.class);

    @ServiceActivator
    public void consumeExistingAndNew(Message<Tweet> message) {
        
    	Tweet tweet = message.getPayload();
       
        System.out.println(tweet.getText() + " from: " + tweet.getFromUser());

    }
}