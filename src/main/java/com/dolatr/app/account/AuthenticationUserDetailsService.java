package com.dolatr.app.account;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.dolatr.app.documents.UserAccount;


public class AuthenticationUserDetailsService implements UserDetailsService {
    
	@Inject
	UserAccountService userAccountService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        UserAccount user = userAccountService.findByUsername(username);
        
        if (user == null)
        	user = userAccountService.findByUserId(username);
        
        throwExceptionIfNotFound(user, username);
      
        return user;
        
    //    return new AuthenticationUserDetails(user);
    }

    private void throwExceptionIfNotFound(UserAccount user, String login) {
        if (user == null) {
            throw new UsernameNotFoundException("User with login " + login + "  has not been found.");
        }
    }
    
    
	
//	 protected Authentication createNewAuthentication(Authentication currentAuth, String newPassword) {
//	        UserDetails user = loadUserByUsername(currentAuth.getName());
//
//	        UsernamePasswordAuthenticationToken newAuthentication =
//	                new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
//	        newAuthentication.setDetails(currentAuth.getDetails());
//
//	        return newAuthentication;
//	    }



    
    
}
