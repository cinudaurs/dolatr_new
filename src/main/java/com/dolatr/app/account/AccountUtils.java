package com.dolatr.app.account;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialUserDetails;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;

public class AccountUtils {
	
	
	private UserAccountRepository userAccountRepository;
	
	
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public UserAccount getLoginUserAccount() {
		
		if (getAuthentication() != null
				&& getAuthentication().getPrincipal() instanceof UserAccount) {
			return (UserAccount) getAuthentication().getPrincipal();
		}
		
		if (getAuthentication() != null
				&& getAuthentication().getPrincipal() instanceof SocialUserDetails) {
			
			//load corresponding local user if it exists
			UserAccount userAccount = userAccountRepository.findByUsername(getAuthentication().getName());
			
			if (userAccount != null)
				return userAccount;
		}
		
		return null;
	}

	public String getLoginUserId() {
		UserAccount account = getLoginUserAccount();
		return (account == null) ? null : account.getUserId();
	}

	public AccountUtils() {
	}

	public AccountUtils(UserAccountRepository userAccountRepository) {
		this.userAccountRepository = userAccountRepository;
	}
}
