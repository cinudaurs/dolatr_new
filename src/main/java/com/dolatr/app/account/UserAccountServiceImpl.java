package com.dolatr.app.account;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.documents.UserSocialConnection;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.repository.UserSocialConnectionRepository;


/**
 * Implementation for AccountService.
 * 
 */
public class UserAccountServiceImpl implements UserAccountService {
    final static Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);

    private final UserAccountRepository accountRepository;
    private final UserSocialConnectionRepository userSocialConnectionRepository;
    private boolean enableAuthorities = true;
    
    @Inject
    public UserAccountServiceImpl(UserAccountRepository accountRepository, UserSocialConnectionRepository 
            userSocialConnectionRepository, UserAdminService userAdminService) {
        this.accountRepository = accountRepository;
        this.userSocialConnectionRepository = userSocialConnectionRepository;
    }


    @Override
    public UserAccount findByUserId(String userId) {
        return accountRepository.findByUserId(userId);
    }

    @Override
    public UserAccount findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserAccount> getAllUsers() {
        return accountRepository.findAll();
    }

    
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<UserAccount> getAllUsers(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    
    @Override
    public List<UserSocialConnection> getConnectionsByUserId(String userId){
        return this.userSocialConnectionRepository.findByUserId(userId);
    }

	
		public boolean userExists(String username) {
	        
			UserAccount userAccount = accountRepository.findByUsername(username);

	        if (userAccount != null) {
	            return true;
	        }

	        return false;
	    }

		
    protected boolean getEnableAuthorities() {
        return enableAuthorities;
    }

    /**
     * Enables loading of authorities (roles) from the authorities table. Defaults to true
     */
    public void setEnableAuthorities(boolean enableAuthorities) {
        this.enableAuthorities = enableAuthorities;
    }
		
}


	
