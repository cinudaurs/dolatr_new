package com.dolatr.app.controller;

import com.dolatr.app.documents.Tweet;

public interface TweetsService {
	
	Tweet read(Long tweetId);

}
