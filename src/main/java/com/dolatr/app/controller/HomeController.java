package com.dolatr.app.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;

@Controller
public class HomeController {
    
    @Inject
	UserAccountRepository accountService;

	@RequestMapping("/")
	public String home(Principal currentUser, Model model) {

//    	model.addAttribute("connectionsToProviders", connectionRepo.findAllConnections());
	//	model.addAttribute(accountRepository.findByUsername(currentUser.getName()));
		return "home";
	}
    
//	@RequestMapping("/rest/tweets/{tweetId}")
//	public String get(Principal currentUser, Model model) {
//
////    	model.addAttribute("connectionsToProviders", connectionRepo.findAllConnections());
//	//	model.addAttribute(accountRepository.findByUsername(currentUser.getName()));
//		return "tweet";
//	}

    
    
	
	
//private final Provider<ConnectionRepository> connectionRepositoryProvider;
//	
//	private final UserAccountRepository accountRepository;
//
//	@Inject
//	public HomeController(Provider<ConnectionRepository> connectionRepositoryProvider, UserAccountRepository accountRepository) {
//		this.connectionRepositoryProvider = connectionRepositoryProvider;
//		this.accountRepository = accountRepository;
//	}
//
//	@RequestMapping("/")
//	public String home(Principal currentUser, Model model) {
//		model.addAttribute("connectionsToProviders", getConnectionRepository().findAllConnections());
//	//	model.addAttribute(accountRepository.findAccountByUsername(currentUser.getName()));
//		return "home";
//	}
//	
//	private ConnectionRepository getConnectionRepository() {
//		return connectionRepositoryProvider.get();
//	}
	

}