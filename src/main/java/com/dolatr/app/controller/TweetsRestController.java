package com.dolatr.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dolatr.app.documents.Tweet;


@Controller
@RequestMapping(value = "rest/tweets")
public class TweetsRestController {
	
	@Autowired
	private TweetsService tweetsService;
	
	@RequestMapping(value = "/{tweetId}", method = RequestMethod.GET)
	public @ResponseBody Tweet read(@PathVariable(value = "tweetId") Long tweetId){
	
		return tweetsService.read(tweetId);
		
	}
	

}
