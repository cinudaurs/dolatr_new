package com.dolatr.app.controller;

import java.security.Principal;

import javax.inject.Inject;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.documents.UserAccount;


@Controller
public class MyAccountController extends AbstractPageController{
	
	@Inject
    UserAccountService userAccountService;
	
	@Inject
	UsersConnectionRepository usersConnectionRepo;
	
		
	@Inject
	ConnectionRepository connectionRepository;

	
	@RequestMapping(value = "/myAccount", method = RequestMethod.GET)
    public String myAccount(Principal currentUser, Model model) {
		
		//reload the user
		UserAccount userAccount = userAccountService.findByUsername(currentUser.getName());

		model.addAttribute("connectionsToProviders", connectionRepository.findAllConnections());
		model.addAttribute("userAccount", userAccount.getUsername());
		
		return "myAccount";
		
  }
	
	   
	    public boolean isLoggedIn() {
	        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        
	        return isAuthenticated(authentication);
	    }
	
	    private boolean isAuthenticated(Authentication authentication) {
	        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
	    }

}
