package com.dolatr.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dolatr.app.documents.Tweet;

@Controller
@RequestMapping(value = "/tweets")
public class RtrvTweetsController {

	@Autowired
	TweetsService tweetsService;
	
	@RequestMapping(value = "/{tweetId}", method = RequestMethod.GET)
	public String getTweet(@PathVariable(value = "tweetId") Long tweetId, Model model){
		
		model.addAttribute("tweetId", tweetId);
		return "tweet";
		
		
	}
	
	
}
