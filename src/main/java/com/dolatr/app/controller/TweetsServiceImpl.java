package com.dolatr.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolatr.app.documents.Tweet;
import com.dolatr.app.repository.TweetRepository;

@Service
public class TweetsServiceImpl implements TweetsService {


	@Autowired
	TweetRepository tweetRepository;
	
	
	@Override
	public Tweet read(Long tweetId) {
		
		return tweetRepository.findByTweetId(tweetId);
	}


	
	
}
