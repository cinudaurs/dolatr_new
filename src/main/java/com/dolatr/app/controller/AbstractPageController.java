package com.dolatr.app.controller;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.dolatr.app.account.AccountUtils;
import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.system.CounterService;

public class AbstractPageController {
    @Inject
    protected UserAccountService accountService;
    @Inject
    protected CounterService counterService;
   
//    @ModelAttribute("loggedinUserAccount")
//    public UserAccount addLoggedinUserAccount() {
//        return AccountUtils.getLoginUserAccount();
//    }
    
    protected String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
        }
        return pathSegment;
    }

    String getUserIpAddress(HttpServletRequest request) {
        return request.getHeader("x-forwarded-for");
    }

}
